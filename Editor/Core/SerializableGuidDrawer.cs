using UnityEngine;
using UnityEditor;


namespace UMNP.GamedevKit.Core
{
    [CustomPropertyDrawer(typeof(SerializableGuid))]
    public class SerializableGuidDrawer : PropertyDrawer
    {
        private static RectOffset _boxPadding = EditorStyles.helpBox.padding;
        private static float _lineHeight = EditorGUIUtility.singleLineHeight;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty stringGuidProperty = property.FindPropertyRelative("stringGuid");
            SerializedProperty guidProperty = property.FindPropertyRelative("guid");
            SerializedProperty isValidProperty = property.FindPropertyRelative("isValid");

            EditorGUI.BeginProperty(position, label, property);

            GUI.Box(EditorGUI.IndentedRect(position), GUIContent.none, EditorStyles.helpBox);
            position = _boxPadding.Remove(position);

            int sceneControlId = GUIUtility.GetControlID(FocusType.Passive);
            position = EditorGUI.PrefixLabel(position, sceneControlId, label);

            position.height = _lineHeight;
            EditorGUI.PropertyField(position, stringGuidProperty, GUIContent.none);

            GUIContent iconContent = new GUIContent();
            position.y += _lineHeight;
            Rect iconRect = position;
            iconRect.width = 20.0f;
            if (isValidProperty.boolValue)
            {
                iconContent = EditorGUIUtility.IconContent("d_winbtn_mac_max");
                EditorGUI.PrefixLabel(iconRect, sceneControlId + 1, iconContent);
                position.width -= iconRect.width;
                position.x += iconRect.width;
                EditorGUI.LabelField(position, "Guid is valid!");
            }
            else
            {
                iconContent = EditorGUIUtility.IconContent("d_winbtn_mac_close");
                EditorGUI.PrefixLabel(iconRect, sceneControlId + 1, iconContent);
                position.width -= iconRect.width;
                position.x += iconRect.width;
                EditorGUI.LabelField(position, "Guid is invalid!");
            }

            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return _boxPadding.vertical + 2.0f * _lineHeight;
        }
    }
}
