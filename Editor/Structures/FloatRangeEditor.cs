using UnityEditor;
using UnityEngine;

namespace UMNP.GamedevKit.Structures
{
    [CustomPropertyDrawer(typeof(FloatRange))]
    public class FloatRangeEditor : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var min = property.FindPropertyRelative("_min");
            var max = property.FindPropertyRelative("_max");

            EditorGUI.BeginProperty(position, label, property);
            {
                var rect = position;
                rect.width = position.width * 0.3f;
                EditorGUI.LabelField(rect, label);

                var contentWidth = position.width - rect.width;
                var halfContentWidth = contentWidth * 0.5f;
                rect.x += rect.width;
                rect.width = 30.0f;
                EditorGUI.LabelField(rect, "Min:");

                rect.x += rect.width;
                rect.width = halfContentWidth - rect.width;
                min.floatValue = Mathf.Min(EditorGUI.FloatField(rect, min.floatValue), max.floatValue);

                rect.x += rect.width;
                rect.width = 30.0f;
                EditorGUI.LabelField(rect, "Max:");

                rect.x += rect.width;
                rect.width = halfContentWidth - rect.width;
                max.floatValue = Mathf.Max(EditorGUI.FloatField(rect, max.floatValue), min.floatValue);
            }
            EditorGUI.EndProperty();
        }
    }
}
