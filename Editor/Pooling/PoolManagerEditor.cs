using UnityEditor;
using UnityEngine;

namespace UMNP.GamedevKit.Pooling
{
    [CustomEditor(typeof(PoolManager))]
    public class PoolManagerEditor : Editor
    {
        private PoolManager _poolManager = null;

        public override void OnInspectorGUI()
        {
            _poolManager = target as PoolManager;

            base.OnInspectorGUI();

            if (GUILayout.Button("Update ID"))
            {
                _poolManager.UpdateID();
                GUIUtility.ExitGUI();
            }
        }
    }
}
