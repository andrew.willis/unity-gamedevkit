using UnityEngine;

namespace UMNP.GamedevKit.UI
{
    public class NotGenuine : MonoBehaviour
    {
        [SerializeField] private Configs.App _appConfig;

        public void OpenStorePage()
        {
            Application.OpenURL(_appConfig.PlayStoreLink);
            Application.Quit();
        }
    }
}
