using System;

namespace UMNP.GamedevKit.Services
{
    public static class DeveloperTools
    {
        private static UI.DeveloperPanel _panelInstance;

        public static void Initialize(UI.DeveloperPanel panel)
        {
            if (_panelInstance) return;
            _panelInstance = panel;
        }

        public static void AddLabel(in string id, in string label)
        {
            if (!_panelInstance) return;
            _panelInstance.AddLabel(id, label);
        }

        public static void AddButtonLabel(in string id, in string label, in string buttonLabel, Action callback)
        {
            if (!_panelInstance) return;
            _panelInstance.AddButtonLabel(id, label, buttonLabel, callback);
        }

        public static void RemoveEntry(in string id)
        {
            if (!_panelInstance) return;
            _panelInstance.RemoveEntry(id);
        }
    }
}
