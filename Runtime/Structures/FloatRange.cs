using System;
using UnityEngine;

namespace UMNP.GamedevKit.Structures
{
    [Serializable]
    public class FloatRange
    {
        [SerializeField] private float _min;
        [SerializeField] private float _max;

        public float Min
        {
            get => _min;
            set => Mathf.Min(value, _max);
        }
        public float Max
        {
            get => _max;
            set => Mathf.Max(value, _min);
        }
        public float Delta => _max - _min;

        public FloatRange() { }
        public FloatRange(float min, float max)
        {
            _min = min;
            _max = max;
        }
    }
}
