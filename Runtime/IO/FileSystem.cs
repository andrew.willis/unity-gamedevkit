using System.IO;

namespace UMNP.GamedevKit.IO
{
    public static class FileSystem
    {
        public static void CreateDirectory(in string path)
        {
            if (Directory.Exists(path)) return;
            Directory.CreateDirectory(path);
        }

        public static bool FileExists(in string path) => File.Exists(path);

        public static string Read(in string path)
        {
            if (!File.Exists(path))
                return string.Empty;

            return File.ReadAllText(path);
        }

        public static byte[] ReadBytes(in string path) => File.ReadAllBytes(path);
        public static void Write(in string path, in string data) => File.WriteAllText(path, data);
        public static void Write(in string path, in byte[] data) => File.WriteAllBytes(path, data);
    }
}

