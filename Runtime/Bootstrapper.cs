using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace UMNP.GamedevKit
{
    public abstract class Bootstrapper : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private Configs.App _appConfig;
        [SerializeField] private UI.Loading _loading;
        [SerializeField] private UI.DeveloperPanel _developerPanel;
        [SerializeField] private GameObject _notGenuine;
        [Header("Parameters")]
        [SerializeField] private int _waitAfterFinished;

        private UI.Loading _loadingInstance;
        private List<Func<Task<Structures.Status>>> _procedures = new List<Func<Task<Structures.Status>>>();

        private void Start()
        {
#if UNITY_EDITOR
#else
            if (_appConfig.Build != Configs.App.BuildType.Debug)
            {
                var genuine = Utils.GenuineChecker.Check(_appConfig.Id, false);
                if (!genuine.success)
                {
                    Debug.Log(genuine.message);
                    Instantiate(_notGenuine);
                    return;
                }
            }
#endif

            BuildProcedures(_procedures, _appConfig);
            InitializeAsync();
        }

        /// <summary>Populate the passed parameter with async initialization callbacks.</summary>
        protected abstract void BuildProcedures(List<Func<Task<Structures.Status>>> procedures, Configs.App appConfig);
        protected async void InitializeAsync()
        {

            if (!_loadingInstance)
                _loadingInstance = Instantiate<UI.Loading>(_loading);

            var initialization = await InitializeAllAsync();

            if (!initialization.success)
            {
                OnFailure(initialization.message);
                return;
            }

            OnSuccess();
            Destroy(_loadingInstance.gameObject);
            Destroy(gameObject);
        }

        protected virtual void InitializeDeveloperTools()
        {
            if (_appConfig.Build == Configs.App.BuildType.Release) return;

            Services.DeveloperTools.Initialize(Instantiate<UI.DeveloperPanel>(_developerPanel));
            Services.DeveloperTools.AddLabel("app_id", $"app_id: {_appConfig.Id}");
            Services.DeveloperTools.AddLabel("app_version_code", $"app_version_code: {_appConfig.VersionCode}");
            Services.DeveloperTools.AddLabel("app_version_name", $"app_version_name: {_appConfig.Version}");
        }

        /// <summary>When any of the procedures failed, this will be invoked. Most common failure handling is to ask the user for a confirmation before retrying.</summary>
        protected abstract void OnFailure(string message);
        /// <summary>When all of the procedures succeeded, this will be invoked. This callback can be implemented with, for example, loading the main menu of the game.</summary>
        protected abstract void OnSuccess();

        /// <summary>
        /// Converts the passed callback into an async method to be used in InitializeAllAsync.
        /// </summary>
        protected Func<Task<Structures.Status>> WrapCallback(Action callback)
        {
            async Task<Structures.Status> AsyncCallback()
            {
                await Task.Yield();
                callback.Invoke();
                return Structures.Status.Success;
            }

            return AsyncCallback;
        }

        private async Task<Structures.Status> InitializeAllAsync()
        {
            _loadingInstance.TotalSteps = _procedures.Count;

            foreach (var procedure in _procedures)
            {
                var status = await procedure();
                if (!status.success) return status;
                _loadingInstance.CurrentStep++;
            }

            await Task.Delay(_waitAfterFinished);

            return Structures.Status.Success;
        }
    }
}
