using System;
using UnityEngine;

namespace UMNP.GamedevKit.GameplayHelpers
{
    [RequireComponent(typeof(Camera))]
    public class CameraControl2D : MonoBehaviour
    {
        [SerializeField, Range(0.0f, 5.0f)] private float _dragSpeed = 1.0f;
        [SerializeField, Range(0.0f, 1.0f)] private float _zoomSpeed = 0.1f;
        [SerializeField] private Vector2 _zoomRange = new Vector2(1.0f, 30.0f);

        private Camera _camera;
        private Vector3 _cameraOriginalPosition;
        private Vector3 _mouseOriginalPosition;
        private Transform _transform;

        public float DragSpeed { get => _dragSpeed; set => _dragSpeed = value; }
        public float ZoomSpeed { get => _zoomSpeed; set => _zoomSpeed = value; }

        /// <summary>
        /// x is minimum, y is maximum.
        /// </summary>
        public Vector2 ZoomRange
        {
            get => _zoomRange;
            set
            {
                var range = new Vector2(Mathf.Max(float.Epsilon, value.x), Mathf.Max(float.Epsilon, value.y));
                _zoomRange = range;
            }
        }

        private void Awake()
        {
            _camera = GetComponent<Camera>();
            _transform = transform;
        }

        public void Drag(in Vector3 mousePosition)
        {
            var offset = mousePosition - _mouseOriginalPosition;

            offset.x /= (float)Screen.width;
            offset.y /= (float)Screen.height;
            offset *= _dragSpeed;
            offset *= _camera.orthographicSize * 2; // I don't know why this must be multiplied by 2 tbh
            offset.x *= _camera.aspect;

            _transform.position = _cameraOriginalPosition - offset;
        }

        public void ReleaseDrag(in Vector3 mousePosition)
        {
            _mouseOriginalPosition = mousePosition;
            _cameraOriginalPosition = _transform.position;
        }

        public void ZoomIn() => Zoom(-_camera.orthographicSize * _zoomSpeed);
        public void ZoomOut() => Zoom(_camera.orthographicSize * _zoomSpeed);

        public void Zoom(float amount) =>
            _camera.orthographicSize = Mathf.Clamp(_camera.orthographicSize + amount, _zoomRange.x, _zoomRange.y);
    }
}
