using UnityEngine;

namespace UMNP.GamedevKit.Configs
{
    [CreateAssetMenu(menuName = "GamedevKit/App", fileName = "New App")]
    public class App : ScriptableObject
    {
        [SerializeField] private BuildType _buildType;
        [SerializeField] private string _playStoreLink;
        [SerializeField] private string _privacyPolicyLink;
        [SerializeField] private string _saveDataFilename;
        [SerializeField] private string _preferencesDataFilename;
        [SerializeField, Tooltip("Will be ignored on mobile")] private int _versionCode;

        public string Id => Application.identifier;
        public BuildType Build => _buildType;
        public string PlayStoreLink => _playStoreLink;
        public string PrivacyPolicyLink => _privacyPolicyLink;
        public string SaveDataFilename => _saveDataFilename;
        public string PreferencesDataFilename => _preferencesDataFilename;
        public string Version => Application.version;
        public int VersionCode
        {
            get
            {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_LINUX || UNITY_STANDALONE_OSX
                return _versionCode;
#elif UNITY_ANDROID
                //int vesioncode =  context().getPackageManager().getPackageInfo(context().getPackageName(), 0).versionCode;
                var contextCls = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                var context = contextCls.GetStatic<AndroidJavaObject>("currentActivity");
                var packageMngr = context.Call<AndroidJavaObject>("getPackageManager");
                var packageName = context.Call<string>("getPackageName");
                var packageInfo = packageMngr.Call<AndroidJavaObject>("getPackageInfo", packageName, 0);
                return packageInfo.Get<int>("versionCode");
#endif
            }
        }

#if UNITY_EDITOR
        [ContextMenu("SyncVersionCode")]
        public void SyncVersionCode() =>
            _versionCode = UnityEditor.PlayerSettings.Android.bundleVersionCode;
#endif

        //int versionName =  context().getPackageManager().getPackageInfo(context().getPackageName(), 0).versionName;
        // public static string VersionName
        // {
        //     get
        //     {
        //         var contextCls = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        //         var context = contextCls.GetStatic<AndroidJavaObject>("currentActivity");
        //         var packageMngr = context.Call<AndroidJavaObject>("getPackageManager");
        //         var packageName = context.Call<string>("getPackageName");
        //         var packageInfo = packageMngr.Call<AndroidJavaObject>("getPackageInfo", packageName, 0);
        //         return packageInfo.Get<string>("versionName");
        //     }
        // }

        public enum BuildType
        {
            /// <summary>Normal development build. Logs as usual.</summary>
            Development,
            /// <summary>Log everything including trace level.</summary>
            Debug,
            /// <summary>Only logs errors and exceptions.</summary>
            Release
        }
    }
}
