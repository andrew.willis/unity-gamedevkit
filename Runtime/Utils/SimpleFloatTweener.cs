using System;

namespace UMNP.GamedevKit.Utils
{
    /// <remarks>Remember to call Update(deltaTime) because this is not a MonoBehaviour</remarks>
    public class SimpleFloatTweener
    {
        public float duration;
        public Easings.Type ease;
        public float end;
        public Action<float> setter;
        public float start;

        public event Action OnStart;
        public event Action OnComplete;

        private float _timeElapsed;
        private bool _isActive;

        public SimpleFloatTweener() { }
        public SimpleFloatTweener(float start, float end, float duration, Easings.Type ease, Action<float> setter)
        {
            this.start = start;
            this.end = end;
            this.duration = duration;
            this.ease = ease;
            this.setter = setter;
        }

        public void Start()
        {
            if (_isActive)
                _timeElapsed = 0.0f;

            _isActive = true;
            OnStart?.Invoke();
        }

        public void Stop()
        {
            _isActive = false;
            _timeElapsed = 0.0f;
            OnComplete?.Invoke();
        }

        public void Update(float deltaTime)
        {
            if (!_isActive) return;

            _timeElapsed += deltaTime;

            var progress = _timeElapsed / duration;
            var ease = Easings.Ease(progress, this.ease);
            setter?.Invoke(CalculateEasedValue(ease));

            if (_timeElapsed < duration) return;
            Stop();
        }

        private float CalculateEasedValue(float ease)
        {
            var delta = end - start;
            return start + ((end - start) * ease);
        }
    }
}
