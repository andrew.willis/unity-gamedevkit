using UMNP.GamedevKit.Structures;
using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    public static class GenuineChecker
    {
        // public static bool Check(in string packageName, bool ignore)
        // {
        //     if (!Application.genuineCheckAvailable)
        //         return false;

        //     if (!Application.genuine)
        //         return false;

        //     if (Application.identifier != packageName)
        //         return false;

        //     if (Application.installerName != "com.android.vending")
        //     {
        //         if (ignore)
        //             return true;

        //         return false;
        //     }

        //     return true;
        // }

        public static Status Check(in string packageName, bool ignore)
        {
            var status = new Status();

            if (!Application.genuineCheckAvailable)
            {
                status.message += "Genuine Check Failed!";
                return status;
            }

            status.message += "Application Integrity Check : ";

            if (!Application.genuine)
            {
                status.message += "[FAIL] " + "\n";
                status.message += "Somebody toucha my spaghet";
                return status;
            }
            else
                status.message += "[PASS] " + "\n";

            status.message += "Application Identifier Check : ";

            if (Application.identifier != packageName)
            {
                status.message += "[FAIL] \n";
                status.message += "Identifier Changed, from " + packageName + " To " + Application.identifier;
                return status;
            }
            else
                status.message += "[PASS] \n";

            status.message += "Installation Check : ";

            var installer = Application.installerName;
            if (installer == "com.android.vending")
            {
                status.message += "Google Play ";
                status.message += "[PASS] \n";
                status.success = true;
                return status;
            }

            status.message += "Unknown installer, Installer Name : \n";
            status.message += installer + " ";
            if (!ignore)
            {
                status.message += "[FAIL] \n";
                return status;
            }
            else
            {
                status.message += "[WARN] \n";
                status.success = true;
                return status;
            }
        }
    }
}
