using System;
using System.Text;

namespace UMNP.GamedevKit.Utils
{
    public static class Conversions
    {
        public static byte[] ToBytes(in string data) => Encoding.UTF8.GetBytes(data);
        public static string ToString(in byte[] data) => Encoding.UTF8.GetString(data);

        public static DateTime ToDateTime(long unixTimestamp)
        {
            // Count from epoch
            var dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return dateTime.AddSeconds(unixTimestamp).ToLocalTime();
        }
    }
}
