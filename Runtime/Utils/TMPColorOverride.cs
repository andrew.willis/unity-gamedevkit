using TMPro;
using UnityEngine;

namespace UMNP.GamedevKit.Utils
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TMPColorOverride : MonoBehaviour
    {
        [SerializeField] private Color _overrideColor = Color.white;

        private TextMeshProUGUI _text;
        private Color _originalColor;

        public Color OverrideColor { get => _overrideColor; set => _overrideColor = value; }

        private void Awake()
        {
            _text = GetComponent<TextMeshProUGUI>();
            _originalColor = _text.color;
        }

        public void Override() =>
            _text.color = _overrideColor;

        public void ResetOverride() =>
            _text.color = _originalColor;
    }
}
