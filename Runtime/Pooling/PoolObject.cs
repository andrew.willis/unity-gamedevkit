using System;
using UMNP.GamedevKit.Utils;
using UnityEngine;

namespace UMNP.GamedevKit.Pooling
{
    /// <remarks>
    /// To use the pooling system:<br/>
    /// 1. Inherit from this class.<br/>
    /// 2. Add the component to the prefab<br/>
    /// 3. Register the prefab to PoolManager<br/>
    /// 4. Hit UpdateID button to update the IDs.
    /// </remarks>
    [DisallowMultipleComponent]
    public abstract class PoolObject : MonoBehaviour
    {
        // This must be serialized to the prefab file.
        [Tooltip("Pool id of this object. Make sure this object is registered to PoolManager and and press UpdateID button to generate the id.")]
        [SerializeField, ReadOnly] private int _id = -1;

        public int Id { get => _id; internal set => _id = value; }

        private Coroutine _despawnCoroutine = null;
        private Action _onDespawn = null;
        private Action _onSpawn = null;

        public void Despawn()
        {
            _onDespawn?.Invoke();
            OnDespawn();
            if (_despawnCoroutine != null)
            {
                StopCoroutine(_despawnCoroutine);
                _despawnCoroutine = null;
            }

            PoolManager.Instance.Return(Id, gameObject);
        }

        /// <summary>
        /// Despawn after given seconds, instead of immediately.
        /// </summary>
        public void Despawn(float seconds) => _despawnCoroutine = StartCoroutine(Coroutines.DoAfter(Despawn, seconds));

        /// <summary>
        /// Similar to MonoBehaviour.OnDestroy, but is instead called when despawned.
        /// </summary>
        /// <remarks>
        /// To be called from outside the class. Override PoolObject.OnDespawn() for inside class usage.
        /// </remarks>
        public void SetOnDespawn(in Action onDespawn) => _onDespawn = onDespawn;

        /// <summary>
        /// Similar to MonoBehaviour.OnStart, but is instead called when spawned from PoolManager.
        /// </summary>
        /// <remarks>
        /// To be called from outside the class. Override PoolObject.OnSpawn() for inside class usage.
        /// </remarks>
        public void SetOnSpawn(in Action onSpawn) => _onSpawn = onSpawn;

        public GameObject Spawn(Transform parent = null, bool worldPositionStays = true)
        {
            var instance = PoolManager.Instance.Spawn(_id, parent, worldPositionStays);

            var poolObject = instance.GetComponent<PoolObject>();
            poolObject.OnSpawn();
            poolObject._onSpawn?.Invoke();

            return instance;
        }

        public GameObject Spawn(in Vector3 position, in Quaternion rotation, Transform parent = null, bool worldPositionStays = true)
        {
            var instance = Spawn(parent, worldPositionStays);
            var instanceTransform = instance.transform;

            instanceTransform.position = position;
            instanceTransform.rotation = rotation;

            return instance;
        }

        public GameObject Spawn(in Vector3 position, in Vector3 localEulerAngles, Transform parent = null, bool worldPositionStays = true)
        {
            var instance = Spawn(parent, worldPositionStays);
            var instanceTransform = instance.transform;

            instanceTransform.position = position;
            instanceTransform.localEulerAngles = localEulerAngles;

            return instance;
        }

        public GameObject Spawn(in Vector3 position, in Vector3 localEulerAngles, in Vector3 localScale, Transform parent = null, bool worldPositionStays = true)
        {
            var instance = Spawn(parent, worldPositionStays);
            var instanceTransform = instance.transform;

            instanceTransform.position = position;
            instanceTransform.localEulerAngles = localEulerAngles;
            instanceTransform.localScale = localScale;

            return instance;
        }

        public T Spawn<T>(Transform parent = null, bool worldPositionStays = true) where T : PoolObject =>
            Spawn(parent, worldPositionStays).GetComponent<T>();

        public T Spawn<T>(in Vector3 position, in Quaternion rotation, Transform parent = null, bool worldPositionStays = true) where T : PoolObject
        {
            var instance = Spawn(parent, worldPositionStays).GetComponent<T>();
            var instanceTransform = instance.transform;

            instanceTransform.position = position;
            instanceTransform.rotation = rotation;

            return instance;
        }

        public T Spawn<T>(in Vector3 position, in Vector3 localEulerAngles, Transform parent = null, bool worldPositionStays = true) where T : PoolObject
        {
            var instance = Spawn(parent, worldPositionStays).GetComponent<T>();
            var instanceTransform = instance.transform;

            instanceTransform.position = position;
            instanceTransform.localEulerAngles = localEulerAngles;

            return instance;
        }

        public T Spawn<T>(in Vector3 position, in Vector3 localEulerAngles, in Vector3 localScale, Transform parent = null, bool worldPositionStays = true) where T : PoolObject
        {
            var instance = Spawn(parent, worldPositionStays).GetComponent<T>();
            var instanceTransform = instance.transform;

            instanceTransform.position = position;
            instanceTransform.localEulerAngles = localEulerAngles;
            instanceTransform.localScale = localScale;

            return instance;
        }

        protected virtual void OnDespawn() { }
        protected virtual void OnSpawn() { }
    }
}
