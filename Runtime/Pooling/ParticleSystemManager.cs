using System.Collections.Generic;

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

using UMNP.GamedevKit.Core;

namespace UMNP.GamedevKit.Pooling
{
    public class ParticleSystemManager : Singleton<ParticleSystemManager>
    {
        [SerializeField] private GameObject[] _poolItems = new GameObject[] { };

        private Dictionary<int, ParticleSystemPoolObject> _pools = new Dictionary<int, ParticleSystemPoolObject>();

        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(this);
        }

        public void Clear(int id)
        {
            if (!_pools.ContainsKey(id))
                return;

            // Debug.Log($"{_pools[id].gameObject}");
            Destroy(_pools[id].gameObject);
            _pools.Remove(id);
        }

        public void ClearAll()
        {
            foreach (var keyValuePair in _pools)
                Destroy(keyValuePair.Value.gameObject);

            _pools.Clear();
        }

        public ParticleSystemPoolObject GetInstance(int id)
        {
            if (!IsReady(id))
                CreatePool(id);

            return _pools[id];
        }

        public bool IsReady(int id)
        {
            if (!_pools.ContainsKey(id))
                return false;

            if (!_pools[id])
                return false;

            return true;
        }

        public void Load(int id)
        {
            if (IsReady(id))
                return;

            CreatePool(id);
        }

        public void StopAll()
        {
            foreach (var particleSystemObject in _pools.Values)
                particleSystemObject.Stop();
        }

#if UNITY_EDITOR
        public void UpdateID()
        {
            if (_poolItems.Length == 0)
                Debug.Log("No pool items registered yet.", this);

            for (var i = 0; i < _poolItems.Length; i++)
            {
                var poolItem = _poolItems[i];
                if (poolItem == null)
                {
                    Debug.LogWarning($"Entry at index {i} is null, make sure every entry is assigned properly.", this);
                    continue;
                }

                var path = AssetDatabase.GetAssetPath(poolItem);
                var prefab = PrefabUtility.LoadPrefabContents(path);

                prefab.GetComponent<ParticleSystemPoolObject>().Id = i;

                PrefabUtility.SaveAsPrefabAsset(prefab, path);
                PrefabUtility.UnloadPrefabContents(prefab);
            }
        }
#endif // UNITY_EDITOR

        private void CreatePool(int id)
        {
            var poolObject = Instantiate(_poolItems[id], transform);
            var particleSystemObject = poolObject.GetComponent<ParticleSystemPoolObject>();

#if UNITY_EDITOR
            if (!particleSystemObject)
            {
                Debug.LogError($"ParticleSystemPoolObject not found in pool item ({id}): {_pools[id].name}", this);
                Destroy(poolObject);
                return;
            }
#endif
            _pools.Add(id, particleSystemObject);
        }
    }
}
