# umnp-gamedevkit

Collection of tools and plugins to help with game development projects.

Current Unity version : Unity 2021.2.7f1

Compatibility with other Unity version is unknown, it is advisable to use the current Unity version this project is using or newer.

## Optional Packages

Packages below are not required dependencies, but I recommend checking them out.

- [SceneReference](https://openupm.com/packages/com.johannesmp.unityscenereference/) by JohannesMP

        openupm add com.johannesmp.unityscenereference

- [Shapes](https://assetstore.unity.com/packages/tools/particles-effects/shapes-173167) by Freya Holmer
- [Procedural UI Image](https://assetstore.unity.com/packages/tools/gui/procedural-ui-image-52200) by Josh H.
- [True Shadow](https://assetstore.unity.com/packages/tools/gui/true-shadow-ui-soft-shadow-and-glow-205220) by Tai's Assets
- Post Processing Stack (v2). Search in Package Manager, under Unity Registries.

## Other Resources

- [Unity Editor icons list](https://github.com/halak/unity-editor-icons)
